import sys


# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file = open(inputFileName, "r")
    line = file.readline()
    file.close()

    return True, line


# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    i = 0
    j = 0
    length = len(password)
    lunghezza = len(text)
    cripto = 0 #dichiaro una variabile in cui insersco i valori ASCII delle lettere
    encryptedText = '' #inizializzo la stringa come vuota
    for i in range(lunghezza): #faccio un ciclo per leggere il testo e criptarlo
        if j < length: #se j è minore della lunghezza leggo semplicemente la password
            cripto = int(ord(text[i]) + ord(password[j])) #faccio casting a int perchè i char vengano identificati da numeri
            if cripto > 126: #utilizzo tabella ASCII non estesa
                cripto -= 95 #tolgo 95 perchè i primi 31 caratteri non sono stampabili

            encryptedText+= chr(cripto) #sostituisco il carattere corrispondente facendo la somma di stringhe
            j += 1
        else:
            j = 0 #nel caso in cui j superasse la lunghezza della password inizio a leggerla di nuovo
            cripto = int(ord(text[i]) + ord(password[j]))
            if cripto > 126:
                cripto -= 95
            encryptedText+= chr(cripto)
            j += 1


    return True, encryptedText


# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    i = 0
    j = 0
    length = len(password)
    lunghezza = len(text)
    decripto = 0 #dichiaro una variabile per il decriptaggio
    decryptedText = ''
    for i in range(lunghezza):
        if j < length:
            decripto = int(ord(text[i]) - ord(password[j]))
            if decripto < 32: #verifico che il carattere faccia parte di quelli stampabili
                decripto += 95
            decryptedText += chr(decripto)
            j += 1
        else:
            j = 0
            decripto = int(ord(text[i]) - ord(password[j]))
            if decripto < 32:
                decripto += 95
            decryptedText += chr(decripto)
            j +=1

    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    encryptedText: object
    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
