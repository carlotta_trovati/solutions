#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt((_a*_a+_b*_b)/2);
  }

  Triangle::Triangle() {
      points.resize(3);
      for(unsigned int i=0;i<points.size();i++)
      {
          points[i].X=0.0;
          points[i].Y=0.0;
  }

  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter=0;
    for(int i=0; i<points.size(); i++)
    {
        int lunghezza=points.size();
        perimeter+=sqrt((points[i%lunghezza].X-points[(i+1)%lunghezza].X)*(points[i%lunghezza].X-points[(i+1)%lunghezza].X)+
                (points[i%lunghezza].Y-points[(i+1)%lunghezza].Y)*(points[i%lunghezza].Y-points[(i+1)%lunghezza].Y));

    }

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,const double& edge):Triangle(p1,p1,p1)
  {
    points[1].X -= (0.5 * edge);
    points[1].Y -= (0.5 * sqrt(3) * edge);
    points[2].X += (0.5 * edge);
    points[2].Y =  points[1].Y;
  }



  Quadrilateral::Quadrilateral() {
      points.resize(4);
      for(unsigned int i=0;i<points.size();i++)
      {
          points[i].X=0.0;
          points[i].Y=0.0;
  }
  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
      AddVertex(p4);

  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for(int i=0; i<points.size(); i++)
    {
        int lunghezza=points.size();
        perimeter+=sqrt((points[i%lunghezza].X-points[(i+1)%lunghezza].X)*(points[i%lunghezza].X-points[(i+1)%lunghezza].X)+
                (points[i%lunghezza].Y-points[(i+1)%lunghezza].Y)*(points[i%lunghezza].Y-points[(i+1)%lunghezza].Y));

    }
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height): Quadrilateral(p1, p1, p1, p1)
  {
      points[1].X = points[0].X+ base;
      points[1].Y = points[0].Y;
      points[2].X = points[1].X;
      points[2].Y = points[1].Y+ height;
      points[3].X = points[0].X;
      points[3].Y = points[2].Y;
  }

  Point Point::operator+(const Point& point) const
  {
    return Point(X+point.X, Y+point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
    return Point(X-point.X, Y-point.Y);
  }

  Point&Point::operator-=(const Point& point)
  {
      X-=point.X;
      Y-=point.Y;
      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X+=point.X;
      Y+=point.Y;
      return *this;
  }
}
