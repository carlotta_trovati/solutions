# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

BusStation::BusStation(const string &busFilePath) {
    _busFilePath=busFilePath;
}


void BusStation::Load() {
    _numBuses=0;
    _pullman.clear();

    ifstream file;
    file.open(_busFilePath);

    if(file.fail())
         throw runtime_error("Something goes wrong");
    try {
        string line; //variabile stringa per salvare righe lette del file
        getline(file, line); //salto le linee non necessarie (di commento)
             getline(file, line);
             istringstream convertN; //istringstream serve per leggere da una stringa già memorizzata
             convertN.str(line);
             convertN >> _numBuses; //assegna a _numBuses il primo carattere della riga
             getline(file, line);
             getline(file, line);
             _pullman.resize(_numBuses);
             for(int i=0; i<_numBuses; i++) //leggo e assegno tuttue le righe inerenti ai diversi bus
             {   getline(file, line);
                 istringstream converter;
                 converter.str(line);
                 converter >>_pullman[i].Id >> _pullman[i].FuelCost; //assegna a _pullman id e costo del carburante

             }
             file.close();

    }  catch (exception) { // lancio un errore in caso il file non sia stato letto correttamente
        _numBuses = 0;
        _pullman.clear();

        throw runtime_error("Something goes wrong");


    }
}


int BusStation::NumberBuses() const  {
    return _numBuses;
}


const Bus &BusStation::GetBus(const int &idBus) const {
    if (idBus > _numBuses)
    throw runtime_error("Bus " + to_string(idBus) + " does not exists");
    // lancio errore se l'id non esiste (id assegnati con numero propedeutico)

    return _pullman[idBus - 1];}

MapData::MapData(const string &mapFilePath) {
      _mapFilePath=mapFilePath; //costruttore
}

void MapData::Load() {
    _numRoutes=0;  //resetto tutte le varabili
    _numStreets=0;
    _numStop=0;
    _fermate.clear();
    _percorsi.clear();
    _strade.clear();
    _streetsFrom.clear();
    _streetsTo.clear();
    _numStradePercorso.clear();

    ifstream file;
    file.open(_mapFilePath);

    if (file.fail())
      throw runtime_error("Something goes wrong");

    try {
        string line;

        getline(file, line);
        getline(file, line);
        istringstream fermateN; //istringstream serve per leggere da una stringa già memorizzata
        fermateN.str(line);
        fermateN>>_numStop; //memorizzo il numero di fermate

        _fermate.resize(_numStop);
        getline(file, line);
        for(int i=0; i<_numStop; i++)
        {
            getline(file, line);
            istringstream formatoFermata;
            formatoFermata.str(line);
            formatoFermata>>_fermate[i].Id>>_fermate[i].Name>>_fermate[i].Latitude>>_fermate[i].Longitude;
            //memorizzo tutti gli attributi della fermata
        }

        getline(file, line);
        getline(file, line);
        istringstream stradeN;
        stradeN.str(line);
        stradeN>>_numStreets; //memorizzo il numero di strade

        _strade.resize(_numStreets); //ridimensiono i vettori con lunghezza pari al numero di strade
        _streetsFrom.resize(_numStreets);
        _streetsTo.resize(_numStreets);
        getline(file, line);
       for(int i=0; i<_numStreets; i++) //ciclo for per leggere le strade sul file
        {
           getline(file, line);
           istringstream formatoStrade;
           formatoStrade.str(line);
           formatoStrade>>_strade[i].Id>>_streetsFrom[i]>>
           _streetsTo[i]>>_strade[i].TravelTime;
           //memorizzo l'id della strada e il tempo di percorrenza in un vettore di strade
           //memorizzo la strada da cui arriva e quella a cui è diretta in due vettori di
           //interi
       }

        getline(file, line);
        getline(file, line);
        istringstream percorsiN;
        percorsiN.str(line);
        percorsiN>>_numRoutes;

        _percorsi.resize(_numRoutes);
        _numStradePercorso.resize(_numRoutes);
        getline(file, line);
        for(int i=0; i<_numRoutes; i++)
        {
            getline(file, line);
            istringstream formatoPercorsi;
            formatoPercorsi.str(line);
            formatoPercorsi>>_percorsi[i].Id>>_percorsi[i].NumberStreets;
            //memorizzo id del percorso e da qaunte strade è composto
            _numStradePercorso[i].resize(_percorsi[i].NumberStreets);
            for(int j=0; j<_percorsi[i].NumberStreets; j++) //ciclo for per leggere tutte le strade del percorso
            {
                formatoPercorsi>>_numStradePercorso[i][j]; //memorizzo in un vettore di vettori di interi le strde del percorso
            }

        }
        file.close();



    }  catch (exception) {

        _numRoutes=0; //resetto tutte le varabili
        _numStreets=0;
        _numStop=0;
        _fermate.clear();
        _percorsi.clear();
        _strade.clear();
        _streetsFrom.clear();
        _streetsTo.clear();
        _numStradePercorso.clear();

        throw runtime_error("Something goes wrong");
    }
}

int MapData::NumberRoutes() const { //funzioni che restituiscono i numeri di percorsi,strade,fermate
    return _numRoutes; }

int MapData::NumberStreets() const {
    return _numStreets;}

int MapData::NumberBusStops() const {
    return _numStop;}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if (idRoute > _numRoutes)
          throw runtime_error("Route " + to_string(idRoute) + " does not exists");
    // lancio errore se l'id non esiste (id assegnati con numero propedeutico)
        const Route& route = _percorsi[idRoute - 1]; //assegno ad una variabile Route il corrispondente percorso
        //salvato precedentemente nel vettore

        if (streetPosition >= route.NumberStreets)
          throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

        int idStreet = _numStradePercorso[idRoute - 1][streetPosition]; //ricavo l'id della strada dal vettore di vettori

        return _strade[idStreet - 1]; //restituisco la strada corrispondente

}

const Route &MapData::GetRoute(const int &idRoute) const {
    if (idRoute > _numRoutes)
         throw runtime_error("Route " + to_string(idRoute) + " does not exists");

       return _percorsi[idRoute - 1]; }

const Street &MapData::GetStreet(const int &idStreet) const {
    if (idStreet > _numStreets)
          throw runtime_error("Street " + to_string(idStreet) + " does not exists");

        return _strade[idStreet - 1]; }

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if (idStreet > _numStreets)
         throw runtime_error("Street " + to_string(idStreet) + " does not exists");

       int idFrom = _streetsFrom[idStreet - 1];

       return _fermate[idFrom - 1]; }

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if (idStreet > _numStreets)
       throw runtime_error("Street " + to_string(idStreet) + " does not exists");

     int idTo = _streetsTo[idStreet - 1];

     return _fermate[idTo - 1]; } //ritorna la fermata verso cui è diretta la strada

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if (idBusStop > _numStop)
           throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

        return _fermate[idBusStop - 1];
}

RoutePlanner::RoutePlanner(const IMapData &mapData, const IBusStation &busStation): _mapData(mapData), _busStation(busStation) { }


int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int i= 0; i< route.NumberStreets; i++)
      travelTime += _mapData.GetRouteStreet(idRoute, i).TravelTime;
    // calcolo il tempo di percorrenza del percorso (ciclo sul numero di strade)
    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {
    const Bus& bus= _busStation.GetBus(idBus);

    int prezzo=0, tempo=0;
    tempo=ComputeRouteTravelTime(idRoute);
    prezzo=tempo*bus.FuelCost*BusAverageSpeed; //calclo il prezzo in base al bus (spazio=velocità*tempo)
    prezzo=prezzo/3600; //divido per 3600 poichè velocità, tempo, e costo del carburante
    //non hanno unità di misura compatibili

    return prezzo;

}

MapViewer::MapViewer(const IMapData &mapData):_mapData(mapData){ }

string MapViewer::ViewRoute(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

       int i=0;
       ostringstream routeView;
       routeView<< to_string(idRoute)<< ": ";
       for (i; i< route.NumberStreets - 1; i++)
       {
         int idStreet = _mapData.GetRouteStreet(idRoute, i).Id;
         string from = _mapData.GetStreetFrom(idStreet).Name;
         routeView << from<< " -> ";
       }

       int idStreet = _mapData.GetRouteStreet(idRoute, i).Id;
       string from = _mapData.GetStreetFrom(idStreet).Name;
       string to = _mapData.GetStreetTo(idStreet).Name;
       routeView << from<< " -> "<< to;

       return routeView.str(); }

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
        const BusStop& to = _mapData.GetStreetTo(idStreet);

        return to_string(idStreet) + ": " + from.Name + " -> " + to.Name; }

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

       return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")"; }

}
