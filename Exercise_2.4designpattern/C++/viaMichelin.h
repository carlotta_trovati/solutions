#ifndef VIAMICHELIN_H
#define VIAMICHELIN_H

#include <iostream>
#include <exception>

#include "bus.h"
#include <vector>

using namespace std;
using namespace BusLibrary;

namespace ViaMichelinLibrary {

  class BusStation : public IBusStation {
    private:
      string _busFilePath;
      int _numBuses;
      vector<Bus> _pullman;

    public:
      BusStation(const string& busFilePath);

      void Load();
      int NumberBuses() const;
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {
    private:
      int _numRoutes, _numStreets, _numStop;
      string _mapFilePath;
      vector<BusStop> _fermate;
      vector<Route> _percorsi;
      vector<Street> _strade;
      vector<int> _streetsFrom;
      vector<int> _streetsTo;
      vector<vector<int>> _numStradePercorso;
      
    public:
      MapData(const string& mapFilePath);
      void Load();
      int NumberRoutes() const;
      int NumberStreets() const;
      int NumberBusStops() const;
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusAverageSpeed;


  private:
    const IMapData& _mapData;
    const IBusStation& _busStation;


    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation);

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const;
  };

  class MapViewer : public IMapViewer {
  private:
    const IMapData& _mapData;

    public:
      MapViewer(const IMapData& mapData);
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // VIAMICHELIN_H
