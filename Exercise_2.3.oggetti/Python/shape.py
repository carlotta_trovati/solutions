import math
class Point:
    def __init__(self, x: float, y: float):
        self.x=x
        self.y=y



class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center=center
        self.a=a
        self.b=b
    def area(self):
     return self.a*self.b*math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Ellipse, self).__init__()
        self.center=center
        self.radius=radius
    def area(self):
        return Ellipse.area(Ellipse(self.center, self.radius,self.radius))



class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
    def area(self):
        AB=math.sqrt((self.p1.x-self.p2.x)*(self.p1.x-self.p2.x)+(self.p1.y-self.p2.y)*(self.p1.y-self.p2.y))
        BC = math.sqrt((self.p2.x-self.p3.x)*(self.p2.x-self.p3.x)+(self.p2.y-self.p3.y)*(self.p2.y-self.p3.y))
        CA= math.sqrt((self.p3.x-self.p1.x)*(self.p3.x-self.p1.x)+(self.p3.y-self.p1.y)*(self.p3.y-self.p1.y))
        p=(AB+BC+CA)/2
        return math.sqrt(p*(p-AB)*(p-BC)*(p-CA))

class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1=p1
        self.edge=edge
    def area(self):
        return math.sqrt(3)/4*self.edge*self.edge


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
        self.p4=p4
    def area(self):
        AB = math.sqrt(
            (self.p1.x - self.p2.x) * (self.p1.x - self.p2.x) + (self.p1.y - self.p2.y) * (self.p1.y - self.p2.y))
        BC = math.sqrt(
            (self.p2.x - self.p3.x) * (self.p2.x - self.p3.x) + (self.p2.y - self.p3.y) * (self.p2.y - self.p3.y))
        CD = math.sqrt(
            (self.p3.x - self.p4.x) * (self.p3.x - self.p4.x) + (self.p3.y - self.p4.y) * (self.p3.y - self.p4.y))
        DA = math.sqrt(
            (self.p4.x - self.p1.x) * (self.p4.x - self.p1.x) + (self.p4.y - self.p1.y) * (self.p4.y - self.p1.y))
        diag = math.sqrt(
            (self.p3.x - self.p1.x) * (self.p3.x - self.p1.x) + (self.p3.y - self.p1.y) * (self.p3.y - self.p1.y))
        p = (AB + BC + diag) / 2
        p2= (CD+DA+diag)/2
        return math.sqrt(p * (p - AB) * (p - BC) * (p - diag))+math.sqrt(p2*(p2-CD)*(p2-DA)*(p2-diag))



class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super(Quadrilateral, self).__init__()
        self.p1=p1
        self.p2=p2
        self.p4=p4
    def area(self):
        return Quadrilateral.area(Quadrilateral(self.p2,self.p1,self.p4, self.p1))


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1=p1
        self.base=base
        self.height=height
    def area(self):
        return self.base*self.height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Rectangle, self).__init__()
        self.p1=p1
        self.edge=edge
    def area(self):
        return Rectangle.area(Rectangle(self.p1,self.edge, self.edge))
