#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y) {
    _x=x;
    _y=y; }

Ellipse::Ellipse(const Point &center, const int &a, const int &b) {
    _a=a;
    _b=b;
    //_area=M_PI*_a*_b;
}

double Ellipse::Area() const {

    return M_PI*(_a)*(_b); }

Circle::Circle(const Point &center, const int &radius):Ellipse(center,radius,radius) {

 //_area=M_PI*_radius*_radius;
}

double Circle::Area() const {
    return Ellipse::Area(); }

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3): _p1(p1), _p2(p2), _p3(p3)
{

}

double Triangle::Area() const {
    double AB=0, BC=0, CA=0, p=0;
    AB=sqrt((_p1._x-_p2._x)*(_p1._x-_p2._x)+(_p1._y-_p2._y)*(_p1._y-_p2._y));
    BC=sqrt((_p2._x-_p3._x)*(_p2._x-_p3._x)+(_p2._y-_p3._y)*(_p2._y-_p3._y));
    CA=sqrt((_p3._x-_p1._x)*(_p3._x-_p1._x)+(_p3._y-_p1._y)*(_p3._y-_p1._y));
    p=(AB+BC+CA)/2;
    return sqrt(p*(p-AB)*(p-BC)*(p-CA));  }

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
  _edge=edge;
}

double TriangleEquilateral::Area() const { return sqrt(3)/4*_edge*_edge; }

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4): _p1(p1), _p2(p2), _p3(p3), _p4(p4)
{
}

double Quadrilateral::Area() const
{
    double AB=0, BC=0, CD=0, DA=0, perimetro1=0, diag=0, perimetro2=0;
    AB=sqrt((_p1._x-_p2._x)*(_p1._x-_p2._x)+(_p1._y-_p2._y)*(_p1._y-_p2._y));
    BC=sqrt((_p2._x-_p3._x)*(_p2._x-_p3._x)+(_p2._y-_p3._y)*(_p2._y-_p3._y));
    CD=sqrt((_p3._x-_p4._x)*(_p3._x-_p4._x)+(_p3._y-_p4._y)*(_p3._y-_p4._y));
    DA=sqrt((_p4._x-_p1._x)*(_p4._x-_p1._x)+(_p4._y-_p1._y)*(_p4._y-_p1._y));
    diag=sqrt((_p1._x-_p3._x)*(_p1._x-_p3._x)+(_p1._y-_p3._y)*(_p1._y-_p3._y));
    perimetro1=(AB+BC+diag)/2;
    perimetro2=(CD+DA+diag)/2;
    return sqrt(perimetro1*(perimetro1-AB)*(perimetro1-BC)*(perimetro1-diag))
            +sqrt(perimetro2*(perimetro2-CD)*(perimetro2-DA)*(perimetro2-diag));;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):Quadrilateral(p2,p1,p4,p1)
{ }

double Parallelogram::Area() const { return Quadrilateral::Area(); }

Rectangle::Rectangle(const Point &p1, const int &base, const int &height):_p1(p1) {
    _base=base;
    _height=height;
    _p1=p1;
}

double Rectangle::Area() const { return _base*_height; }

Square::Square(const Point &p1, const int &edge):Rectangle(p1, edge,edge)
{

}

double Square::Area() const { return Rectangle::Area(); }




}
