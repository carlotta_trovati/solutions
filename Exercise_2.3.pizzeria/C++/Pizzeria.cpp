#include "Pizzeria.h"

namespace PizzeriaLibrary {

bool Ingredient::operator<(const Ingredient &ingredient) const
{
    return (Name< ingredient.Name);
}

void Pizza::AddIngredient(const Ingredient &ingredient) {
    unsigned int numIngredienti=NumIngredients(), contatore=0;
    for(unsigned int i=0; i<numIngredienti; i++)
    {
        if(ingredient.Name==ingredients[i].Name)
            contatore++;
        if(contatore>2)
            throw runtime_error ("Ingredient already inserted");
    }
    ingredients.push_back(ingredient);}


int Pizza::NumIngredients() const { return ingredients.size();}


int Pizza::ComputePrice() const {
    int prezzo=0;
    for (unsigned int i=0; i<ingredients.size(); i++)
    {
        prezzo+=ingredients[i].Price;
    }
    return prezzo;
}

void Order::InitializeOrder(int numPizzas) { pizzas.reserve(numPizzas); }

void Order::AddPizza(const Pizza &pizza) { pizzas.push_back(pizza); }

const Pizza &Order::GetPizza(const int &position) const {
    if(position<0||position>NumPizzas())
        throw runtime_error("Position passed is wrong");
    return pizzas[position]; }

int Order::NumPizzas() const { return pizzas.size();}

int Order::ComputeTotal() const {
    int prezzo=0;
    for (unsigned int i=0; i<pizzas.size(); i++)
    {
        prezzo+=pizzas[i].ComputePrice();
    }
    return prezzo;}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {
    bool trovato=false;
    unsigned int lunghezza=ingredients.size();
    for(unsigned int i=0;i<lunghezza; i++)
    {
        if(name==ingredients[i].Name)
        {   trovato=true;
            throw runtime_error("Ingredient already inserted");
            break;
        }
    }
    if(trovato==false)
    {
        ingredients.resize(lunghezza+1);
        ingredients[ingredients.size()-1].Name=name;
        ingredients[ingredients.size()-1].Description=description;
        ingredients[ingredients.size()-1].Price=price;
    }
    sort(ingredients.begin(), ingredients.end());
    }

const Ingredient &Pizzeria::FindIngredient(const string &name) const {
    bool trovato=false;
    unsigned int i=0;
    for(i=0; i<ingredients.size(); i++)
    {
        if(name==ingredients[i].Name)
        {
            trovato=true;
            break;
        }
    }
    if (trovato==false)
        throw runtime_error("Ingredient not found");
    else
        return ingredients[i];
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {

    bool trovato=false;
    unsigned int lunghezza=pizzas.size();
    for(unsigned int i=0;i<lunghezza; i++)
    {
        if(name==pizzas[i].Name)
        {   trovato=true;
            throw runtime_error("Pizza already inserted");
            break;
        }
    }
    if(trovato==false)
    {
        pizzas.resize(lunghezza+1);
        pizzas[pizzas.size()-1].Name=name;
        for(unsigned int j=0; j<ingredients.size(); j++)
        {
            pizzas[pizzas.size()-1].AddIngredient(FindIngredient(ingredients[j]));
        }
    }
}

const Pizza &Pizzeria::FindPizza(const string &name) const {
    bool trovato=false;
    unsigned int i=0;
    for(i=0; i<pizzas.size(); i++)
    {
        if(name==pizzas[i].Name)
        {
            trovato=true;
            break;
        }
    }
    if (trovato==false)
        throw runtime_error("Pizza not found");
    else
        return pizzas[i];}

int Pizzeria::CreateOrder(const vector<string> &pizzas) {
    Order ordine;
    unsigned int i=0;
    if(pizzas.size()<=0)
    throw runtime_error("Empty order");
    else
    { ordine.InitializeOrder(pizzas.size());
      for(i=0; i<pizzas.size(); i++)
      {
       Pizza pizza=FindPizza(pizzas[i]);
       ordine.AddPizza(pizza);
      }
      orders.push_back(ordine);
      numOrders++;
      return numOrders-1;
    }


}

const Order &Pizzeria::FindOrder(const int &numOrder) const {
if(numOrder<1000||numOrder>numOrders-1)
    throw runtime_error ("Order not found");
else
{
    return orders[numOrder-1000];
}
}

string Pizzeria::GetReceipt(const int &numOrder) const {
Order ordine=FindOrder(numOrder);
string prezzo, scontrino="", totale;
for(int i=0; i<ordine.NumPizzas(); i++)
{
    Pizza pizza=ordine.GetPizza(i);
    prezzo=to_string(pizza.ComputePrice());
    scontrino+="- "+pizza.Name+", "+prezzo+" euro"+"\n";
}
totale=to_string(ordine.ComputeTotal());
scontrino+="  TOTAL: "+totale+" euro"+"\n";
return scontrino;
}

string Pizzeria::ListIngredients() const {
   string stampa="", prezzo="";
   for(unsigned int i=0;i<ingredients.size();i++)
   {
     prezzo=to_string(ingredients[i].Price);
     stampa +=ingredients[i].Name+" - '"+ingredients[i].Description+"': "+prezzo+" euro"+"\n";
   }
   return stampa;
 }

string Pizzeria::Menu() const {
string menu, numIngredienti, prezzo;
for(int i=pizzas.size()-1; i>=0; i--)
{
 prezzo=to_string(pizzas[i].ComputePrice());
 numIngredienti=to_string(pizzas[i].NumIngredients());
 menu+= pizzas[i].Name+" ("+numIngredienti+" ingredients): "+prezzo+" euro"+"\n";
}
return menu;
}

}
