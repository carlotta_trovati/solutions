class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredients = []

    def addIngredient(self, ingredient: Ingredient):
        self.contatore=0
        for i in range (0,len(self.ingredients)):
            if self.ingredients[i].Name is ingredient.Name:
                self.contatore+=1
            if self.contatore > 2:
                raise Exception("Ingredient already inserted")

        self.ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredients)

    def computePrice(self) -> int:
        prezzo: int = 0
        for i in range(0, len(self.ingredients)):
            prezzo += self.ingredients[i].Price
        return prezzo


class Order:
    def __init__(self):
        self.listpizze = []
        self.numorder=-1

    def getPizza(self, position: int) -> Pizza:
        if (position < 0 or position > self.numPizzas()):
            raise Exception("Position passed is wrong")
        else:
          return self.listpizze[position-1]

    def initializeOrder(self, numPizzas: int):
        if (numPizzas<1):
            raise Exception ("Empty order")
        self.listpizze=[]*numPizzas

    def addPizza(self, pizza: Pizza):
        self.listpizze.append(pizza)

    def numPizzas(self) -> int:
        return len(self.listpizze)

    def computeTotal(self) -> int:
        prezzo:int=0
        for i in range(0, len(self.listpizze)):
            prezzo+= self.listpizze[i].computePrice()
        return prezzo


class Pizzeria:
    def __init__(self):
       self.ingredienti = []
       self.pizza= []
       self.ordini = []
       self.numOrders = 1000

    def addIngredient(self, name: str, description: str, price: int):
        for i in range (0, len(self.ingredienti)):
            if self.ingredienti[i].Name is name:
              raise Exception ("Ingredient already inserted")
        self.ingredienti.append(Ingredient(name, price, description))



    def findIngredient(self, name: str) -> Ingredient:
        trovato: bool = False
        i=0
        for i in range (0, len(self.ingredienti)):
            if self.ingredienti[i].Name is name:
                trovato=True
                break
        if trovato is False:
                raise Exception ("Ingredient not found")
        else:
            return self.ingredienti[i]



    def addPizza(self, name: str, ingredients: []):
        trovato: bool = False
        for i in range (0, len(self.pizza)):
            if self.pizza[i].Name is name:
                trovato= True
                raise Exception ("Pizza already inserted")
        if trovato is False:
            pizza: Pizza = Pizza("")
            pizza.Name =name
            for j in ingredients:
                pizza.ingredients.append(self.findIngredient(j))
            self.pizza.append(pizza)



    def findPizza(self, name: str) -> Pizza:
        trovato: bool = False
        i=0
        for i in range (0, len(self.pizza)):
            if self.pizza[i].Name is name:
                trovato = True
                break
        if trovato is False :
            raise Exception ("Pizza not found")
        else:
            return self.pizza[i]



    def createOrder(self, pizzas: []) -> int:
        order : Order = Order()
        order.initializeOrder(len(pizzas))
        for i in range (0, len(pizzas)):
            order.listpizze.append(self.findPizza(pizzas[i]))
        order.numorder= len(self.ordini)+self.numOrders
        self.ordini.append(order)
        return order.numorder

    def findOrder(self, numOrder: int) -> Order:
        if (numOrder<1000 or numOrder> self.numOrders+ len(self.ordini)-1):
            raise Exception ("Order not found")
        else:
            return self.ordini[numOrder-1000]

    def getReceipt(self, numOrder: int) -> str:
        ordine= self.findOrder(numOrder)
        scontrino = ""
        for i in range (0, ordine.numPizzas()):
            scontrino+= "- " + ordine.listpizze[i].Name + ", " + str(ordine.listpizze[i].computePrice()) + " euro" + "\n"
        scontrino+= "  TOTAL: " + str(ordine.computeTotal()) + " euro" + "\n"
        return scontrino

    def listIngredients(self) -> str:
        stampa = ""
        nomi = []
        for i in range (0, len(self.ingredienti)):
            nomi.append(self.ingredienti[i].Name)
        nomi.sort()
        for j in range (0, len(nomi)):
            stampa+= nomi[j] + " - '" + self.findIngredient(nomi[j]).Description + "': " + str(self.findIngredient(nomi[j]).Price) + " euro" + "\n"
        return stampa

    def menu(self) -> str:
        menu = ""
        for i in range (0, len(self.pizza)):
            menu+= self.pizza[i].Name + " (" + str(self.pizza[i].numIngredients()) + " ingredients): " + str(self.pizza[i].computePrice()) + " euro" + "\n"
        return menu