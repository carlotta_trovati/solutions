```plantuml
@startuml

class Ingredient {
  +Name: string
	+Price: int
	+Description: string
}

class Pizza {
	+Name: string
  +void AddIngredient(Ingredient ingredient)
  +int NumIngredients()
  +int ComputePrice()
}

class Order {
  +Pizza GetPizza(int position)
  +void InitializeOrder(int numPizzas)
  +void AddPizza(Pizza pizza)
  +int NumPizzas()
  +int ComputeTotal()
}

class Pizzeria {
  +void AddIngredient(string name, string description, int price)
  +Ingredient FindIngredient(string name)
  +void AddPizza(string name, vector<string> ingredients)
  +Pizza FindPizza(string name)
  +int CreateOrder(vector<string> pizzas)
  +Order FindOrder(int numOrder)
  +string GetReceipt(int numOrder)
  +string ListIngredients()
  +string Menu()
}

Pizzeria "1" *-- "*" Ingredient: contains
Pizzeria "1" *-- "*" Pizza: contains
Pizzeria "1" *-- "*" Order: contains
Order - Pizza: has many >
Pizza - Ingredient: has many >

@enduml
```