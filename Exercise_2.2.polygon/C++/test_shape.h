#ifndef __TEST_SHAPE_H
#define __TEST_SHAPE_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "shape.h"

using namespace testing;

TEST(TestShape, TestEllipse)
{
  ShapeLibrary::Point center = ShapeLibrary::Point(1.2, 2.5);

  EXPECT_TRUE(fabs(ShapeLibrary::Ellipse(center,
                                         12,
                                         15).Area() - 565.4866776461628) < 1e-6);
}

TEST(TestShape, TestCircle)
{
  ShapeLibrary::Point center = ShapeLibrary::Point(1.2, 2.5);

  EXPECT_TRUE(fabs(ShapeLibrary::Circle(center,
                                        17).Area() - 907.9202768874503) < 1e-6);
}

TEST(TestShape, TestTriangle)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(2.8, 3.9);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(3.9, 1.8);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(3.1, 2.7);

  EXPECT_TRUE(fabs(ShapeLibrary::Triangle(p1,
                                          p2,
                                          p3).Area() - 0.345) < 1e-6);
}

TEST(TestShape, TestTriangleEquilateral)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(1.2, 1.7);

  EXPECT_TRUE(fabs(ShapeLibrary::TriangleEquilateral(p1,
                                                     16).Area() - 110.851251684) < 1e-6);
}

TEST(TestShape, TestQuadrilateral)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(-3.0, 1.0);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(1.0, -2.0);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(3.0, 2.0);
  ShapeLibrary::Point p4 = ShapeLibrary::Point(-1.0, 4.0);


  EXPECT_TRUE(fabs(ShapeLibrary::Quadrilateral(p1,
                                               p2,
                                               p3,
                                               p4).Area() - 19.0) < 1e-6);
}

TEST(TestShape, TestParallelogram)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);
  ShapeLibrary::Point p2 = ShapeLibrary::Point(10.0, 1.0);
  ShapeLibrary::Point p3 = ShapeLibrary::Point(4.0, 3.0);

  EXPECT_TRUE(fabs(ShapeLibrary::Parallelogram(p1,
                                               p2,
                                               p3).Area() - 14.0) < 1e-6);
}

TEST(TestShape, TestRectangle)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);

  EXPECT_TRUE(fabs(ShapeLibrary::Rectangle(p1,
                                           7,
                                           3).Area() - 21.0) < 1e-6);
}

TEST(TestShape, TestSquare)
{
  ShapeLibrary::Point p1 = ShapeLibrary::Point(3.0, 1.0);

  EXPECT_TRUE(fabs(ShapeLibrary::Square(p1,
                                        7).Area() - 49.0) < 1e-6);
}

#endif // __TEST_SHAPE_H
