import math
class Point:
    def __init__(self, x: float, y: float):
        self.x=x
        self.y=y



class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center=center
        self.a=a
        self.b=b
    def area(self):
        return self.a*self.b*math.pi


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Ellipse, self).__init__()
        self.center=center
        self.radius=radius
    def area(self):
        return Ellipse.area(Ellipse(self.center, self.radius,self.radius))



class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
    def area(self):
        AB=math.sqrt((self.p1.x-self.p2.x)*(self.p1.x-self.p2.x)+(self.p1.y-self.p2.y)*(self.p1.y-self.p2.y))
        BC = math.sqrt((self.p2.x-self.p3.x)*(self.p2.x-self.p3.x)+(self.p2.y-self.p3.y)*(self.p2.y-self.p3.y))
        CA= math.sqrt((self.p3.x-self.p1.x)*(self.p3.x-self.p1.x)+(self.p3.y-self.p1.y)*(self.p3.y-self.p1.y))
        p=(AB+BC+CA)/2
        return math.sqrt(p*(p-AB)*(p-BC)*(p-CA))

class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.p2 = Point(p1.x - 0.5*edge, p1.y - 0.5*math.sqrt(3)*edge)
        self.p3 = Point(p1.x + 0.5*edge, self.p2.y)
        super().__init__ (self.p1, self.p2, self.p3)


    def area(self):
        return Triangle.area(Triangle(self.p1, self.p2, self.p3))


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1=p1
        self.p2=p2
        self.p3=p3
        self.p4=p4
    def area(self):
        AB = math.sqrt(
            (self.p1.x - self.p2.x) ** 2 + (self.p1.y - self.p2.y) * (self.p1.y - self.p2.y))
        BC = math.sqrt(
            (self.p2.x - self.p3.x) * (self.p2.x - self.p3.x) + (self.p2.y - self.p3.y) * (self.p2.y - self.p3.y))
        CD = math.sqrt(
            (self.p3.x - self.p4.x) * (self.p3.x - self.p4.x) + (self.p3.y - self.p4.y) * (self.p3.y - self.p4.y))
        DA = math.sqrt(
            (self.p4.x - self.p1.x) * (self.p4.x - self.p1.x) + (self.p4.y - self.p1.y) * (self.p4.y - self.p1.y))
        diag = math.sqrt(
            (self.p3.x - self.p1.x) * (self.p3.x - self.p1.x) + (self.p3.y - self.p1.y) * (self.p3.y - self.p1.y))
        p = (AB + BC + diag) / 2
        p2= (CD+DA+diag)/2
        return math.sqrt(p * (p - AB) * (p - BC) * (p - diag))+math.sqrt(p2*(p2-CD)*(p2-DA)*(p2-diag))



class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.p1=p1
        self.p2=p2
        self.p4=p4
        super().__init__(self.p1, self.p2, self.p1, self.p4)

    def area(self):
        return Quadrilateral.area(Quadrilateral(self.p2,self.p1,self.p4, self.p1))


class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1=p1
        self.base=base
        self.height=height
        self.p2 = Point(self.p1.x + self.base, self.p1.y)
        self.p3 = Point(self.p2.x, self.p2.y + self.height)
        self.p4 = Point(self.p1.x, self.p3.y)

        super(Parallelogram, self).__init__(self.p1, self.p2, self.p3, self.p4)

    def area(self):
        return Parallelogram.area(Parallelogram(self.p1, self.p2, self.p4))


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Rectangle, self).__init__(self, p1, edge)
        self.p1=p1
        self.edge=edge
    def area(self):
        return Rectangle.area(Rectangle(self.p1,self.edge, self.edge))
